//
//  TVShowService.h
//  Test
//
//  Created by German Battiston on 1/2/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PagingResults;

@interface TVShowService : NSObject

- (void)fetchNextPageTopRatedTVShowsWithPaginatedResults:(PagingResults *)paginatedResults completionBlock:(void (^)(void))completionBlock andFailBlock:(void (^)(NSError *error))failBlock;

@end
