//
//  ApiClient.m
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import "ApiClient.h"

@implementation ApiClient

+ (ApiClient *)sharedInstance
{
	static ApiClient *shared;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		
		NSString *baseURLString = @"http://api.themoviedb.org/";
		shared = [[ApiClient alloc] initWithBaseURL:[NSURL URLWithString:baseURLString]];
		
		AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
		securityPolicy.allowInvalidCertificates = YES;
		shared.securityPolicy = securityPolicy;
		
	});
	return shared;
}

- (id)initWithBaseURL:(NSURL *)url
{
	self = [super initWithBaseURL:url];
	if (self)
	{
		self.responseSerializer = [AFJSONResponseSerializer serializer];
		self.requestSerializer = [AFJSONRequestSerializer serializer];
	}
	
	return self;
}

#pragma mark - Base Get Request

// For this example I only needed a GET service
- (void)getRequestWithURL:(NSString *)url parameters:(NSDictionary *)parameters completion:(APIClientResponseBlock)completion
{
	NSString *fullUrl = [NSString stringWithFormat:@"%@%@", self.baseURL.absoluteString, url];
	
	[self GET:fullUrl parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
		
		
	} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
		
		if (responseObject)
		{
			completion(responseObject, nil);
		}
		
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		
		NSLog(@"ERROR: %@", error.description);
		
		completion(nil, error);
		
	}];
}

@end
