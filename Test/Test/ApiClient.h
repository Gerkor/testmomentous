//
//  ApiClient.h
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import <Foundation/Foundation.h>

@import AFNetworking;

typedef void (^APIClientResponseBlock)(NSDictionary *response, NSError *error);

@interface ApiClient : AFHTTPSessionManager

+ (ApiClient *)sharedInstance;

- (id)initWithBaseURL:(NSURL *)url;

#pragma mark - Base Get Request

- (void)getRequestWithURL:(NSString *)url parameters:(NSDictionary *)parameters completion:(APIClientResponseBlock)completion;

@end
