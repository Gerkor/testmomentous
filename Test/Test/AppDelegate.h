//
//  AppDelegate.h
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
