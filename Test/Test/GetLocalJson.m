//
//  GetLocalJson.m
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import "GetLocalJson.h"
#import "Item.h"

#define kLocalJsonData	@"localData"
#define kTitle			@"title"
#define kImageUrl		@"imageURL"
#define kEmpty			@"EMPTY"

@interface GetLocalJson ()

@property (nonatomic, strong) NSMutableArray *data;

@end

@implementation GetLocalJson

// This would be the place where I'd use the ApiClient to GET data from a web service.
- (NSArray *)getDataFromLocalJson
{
	NSError *deserializingError;
	NSString *jsonPath = [[NSBundle mainBundle] pathForResource:kLocalJsonData ofType:@"json"];
	NSData *data = [NSData dataWithContentsOfFile:jsonPath];
	
	NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&deserializingError];
	self.data = [NSMutableArray array];
	for (NSDictionary *dict in json)
	{
		Item *item = [[Item alloc] init];
		
		[item setTitle:[dict valueForKey:kTitle] == nil ? kEmpty : [dict valueForKey:kTitle]];
		[item setPosterPath:[dict valueForKey:kImageUrl]];
		
		[self.data addObject:item];
	}
	
	return self.data;
}

@end
