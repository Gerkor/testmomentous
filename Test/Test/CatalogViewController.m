//
//  CatalogViewController.m
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import "CatalogViewController.h"
#import "ItemTableViewCell.h"
#import "GetLocalJson.h"
#import "TVShowService.h"
#import "Item.h"
#import "MBProgressHUD.h"
#import "PagingResults.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

#define kTitle		@"title"
#define kImageUrl	@"imageURL"
#define kEmpty		@"EMPTY"
#define kViewTitle	@"Tv Show Catalog"

#define kCellHeight 53

@interface CatalogViewController () <UISearchBarDelegate, UISearchResultsUpdating, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *itemData;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *filteredList;

@property (nonatomic, strong) PagingResults *paging;
@property (nonatomic, strong) TVShowService *tvShowService;

@property (nonatomic, assign) BOOL isAscending;

@end

@implementation CatalogViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
	{
		self.title = kViewTitle;
		self.paging = [[PagingResults alloc] init];
		self.tvShowService = [[TVShowService alloc] init];
		self.isAscending = NO;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.filteredList = [NSMutableArray array];
	
	self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
	self.searchController.dimsBackgroundDuringPresentation = NO;
	self.searchController.searchResultsUpdater = self;
	self.searchController.searchBar.delegate = self;
	
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	self.tableView.tableHeaderView = self.searchController.searchBar;
	[self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ItemTableViewCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NSStringFromClass([ItemTableViewCell class])];
	
	self.definesPresentationContext = YES;
	
	UIBarButtonItem *flipButton = [[UIBarButtonItem alloc] initWithTitle:@"Sort"
																   style:UIBarButtonItemStylePlain
																  target:self
																  action:@selector(sortValues:)];
	
	self.navigationItem.rightBarButtonItem = flipButton;
	
	[self loadNextPage];
}

- (IBAction)sortValues:(id)sender
{
	// Switch the ascending and descending sort.
	self.isAscending = !self.isAscending;
	
	self.paging.results = [[self.paging.results sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
		NSString *first = [(Item *)a title];
		NSString *second = [(Item *)b title];
		
		if (self.isAscending)
		{
			return [first compare:second];
		}
		else
		{
			return [second compare:first];
		}
		
	}] mutableCopy];
	
	[self.tableView reloadData];
}

- (void)loadNextPage
{
	__weak typeof(self)weakSelf = self;
	
	if ([weakSelf.paging.currentPage intValue] < 1)
	{
		MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
		hud.mode = MBProgressHUDModeIndeterminate;
		hud.labelText = @"Loading Shows...";
	}
	else
	{
		UIView *loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(weakSelf.tableView.frame), 44)];
		UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		
		[loadingView addSubview:indicator];
		
		[indicator startAnimating];
		
		indicator.center = CGPointMake(CGRectGetWidth(loadingView.frame) / 2, CGRectGetHeight(loadingView.frame) / 2);
		weakSelf.tableView.tableFooterView = loadingView;
	}
	
	// Paging handles the progressive loading of the TV Shows
	[self.tvShowService fetchNextPageTopRatedTVShowsWithPaginatedResults:self.paging completionBlock:^{
		
		weakSelf.paging = self.paging;
		[weakSelf.tableView reloadData];
		
		[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
		weakSelf.tableView.tableFooterView = nil;
		
	} andFailBlock:^(NSError *error) {
		
		NSLog(@"%@", error);
		[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
		weakSelf.tableView.tableFooterView = nil;
		
	}];
}

#pragma Mark - Search Bar Delegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
	[self updateFilteredContentWithSearchText:searchController.searchBar.text];
	
	[self.tableView reloadData];
}

// Given the text typed, we filter the existing data
- (void)updateFilteredContentWithSearchText:(NSString*)searchText
{
	[self.filteredList removeAllObjects];
	for (Item *item in self.paging.results)
	{
		NSRange nameRange = [item.title rangeOfString:searchText options:NSCaseInsensitiveSearch];
		if (nameRange.location != NSNotFound)
		{
			[self.filteredList addObject:item];
		}
	}
}

#pragma Mark - Table View Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (self.filteredList.count > 0)
	{
		return self.filteredList.count;
	}
	else
	{
		return [self.paging.results count];
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return kCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ItemTableViewCell class]) forIndexPath:indexPath];
	
	Item *item;
	if (self.filteredList.count > 0)
	{
		item = [self.filteredList objectAtIndex:indexPath.row];
	}
	else
	{
		item = [self.paging.results objectAtIndex:indexPath.row];
	}
	
	// Obtain the show image according to the API url info.
	NSString *urlWithImage = [NSString stringWithFormat:@"http://image.tmdb.org/t/p/w92%@", item.posterPath];
	NSURL *url = [[NSURL alloc] initWithString:urlWithImage];
	
	cell.titleLabel.text = item.title;
	[cell.itemImageView setImageWithURL:url placeholderImage:nil];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	// When we reach to the end of the table, we load new items from the server.
	if ([indexPath row] == [self.paging.results count] - 1)
	{
		[self loadNextPage];
	}
}

@end
