//
//  Paging.h
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PagingResults : NSObject

@property (nonatomic, strong) NSNumber *currentPage;
@property (nonatomic, strong) NSNumber *totalPages;
@property (nonatomic, strong) NSNumber *totalResults;
@property (nonatomic, strong) NSArray *results;

@end
