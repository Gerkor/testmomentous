 //
//  Item.m
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import "Item.h"

@interface Item ()

@end

@implementation Item

- (instancetype)init
{
	if (self = [super init])
	{
		self.title = @"";
		self.posterPath = @"";
	}
	
	return self;
}

@end
