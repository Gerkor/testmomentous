//
//  GetLocalJson.h
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetLocalJson : NSObject

@property (nonatomic, readonly, strong) NSMutableArray *data;

- (NSArray *)getDataFromLocalJson;

@end
