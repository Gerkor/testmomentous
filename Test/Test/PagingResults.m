//
//  Paging.m
//  Test
//
//  Created by German Battiston on 31/1/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import "PagingResults.h"

@implementation PagingResults

- (id)init
{
	self = [super init];
	if (self)
	{
		self.results = @[];
		self.currentPage = 0;
	}
	return self;
}

@end
