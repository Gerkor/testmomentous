//
//  TVShowService.m
//  Test
//
//  Created by German Battiston on 1/2/16.
//  Copyright © 2016 German Battiston. All rights reserved.
//

#import "TVShowService.h"
#import "PagingResults.h"
#import "ApiClient.h"
#import "Item.h"

@interface TVShowService ()

@property (nonatomic, assign) BOOL isFetching;

@end

@implementation TVShowService

// This service has the paging information needed to make it progressive.
- (void)fetchNextPageTopRatedTVShowsWithPaginatedResults:(PagingResults *)paginatedResults completionBlock:(void (^)(void))completionBlock andFailBlock:(void (^)(NSError *error))failBlock
{
	int pageNumber = [paginatedResults.currentPage intValue] + 1;
	
	if (self.isFetching || pageNumber == [paginatedResults.totalPages intValue]) return;
	
	NSDictionary *parameters = @{@"api_key" : @"6e720a950f31d0ca393294c3fe4828c4", @"page":@(pageNumber)};
	
	[[ApiClient sharedInstance] getRequestWithURL:@"/3/tv/top_rated" parameters:parameters completion:^(NSDictionary *response, NSError *error) {
		
		self.isFetching = YES;
		
		paginatedResults.currentPage = [response valueForKey:@"page"];
		paginatedResults.totalPages = [response valueForKey:@"total_pages"];
		paginatedResults.totalResults = [response valueForKey:@"total_results"];
		
		NSArray *results = [response valueForKey:@"results"];
		NSMutableArray *resultsToSet = [[NSMutableArray alloc] init];
		
		for (NSDictionary *tvShowDict in results)
		{
			Item *item = [[Item alloc] init];
			
			item.title = [tvShowDict valueForKey:@"name"] == nil ? @"" : [tvShowDict valueForKey:@"name"];
			item.posterPath = [tvShowDict valueForKey:@"poster_path"] == nil ? @"" : [tvShowDict valueForKey:@"poster_path"];
			
			[resultsToSet addObject:item];
		}
		
		NSMutableArray *newArray = [NSMutableArray arrayWithArray:paginatedResults.results];
		[newArray addObjectsFromArray:resultsToSet];
		paginatedResults.results = newArray;
		
		self.isFetching = NO;
		completionBlock();
		
	}];
}

@end
